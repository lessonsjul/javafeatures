package com.example.demo;

import jdk.incubator.vector.FloatVector;
import jdk.incubator.vector.VectorSpecies;

public class VectorApiUsage {

    private static final VectorSpecies<Float> SPECIES = FloatVector.SPECIES_64;

    public static void main(String[] args) {
        float[] a = {12.4f, 234.3f, 23.43543f};
        float[] b = {34.5f, 23.4f, 5.34f};
        float[] c = {456.43f, 98.23f};
        newVectorComputation(a, b, c);
    }

    public static void newVectorComputation(float[] a, float[] b, float[] c) {
        for (var i = 0; i < a.length; i += SPECIES.length()) {
            var m = SPECIES.indexInRange(i, a.length);
            var va = FloatVector.fromArray(SPECIES, a, i, m);
            var vb = FloatVector.fromArray(SPECIES, b, i, m);
            var vc = va.mul(vb);
            vc.intoArray(c, i, m);
        }
    }

    public void commonVectorComputation(float[] a, float[] b, float[] c) {
        for (var i = 0; i < a.length; i ++) {
            c[i] = a[i] * b[i];
        }
    }
}
