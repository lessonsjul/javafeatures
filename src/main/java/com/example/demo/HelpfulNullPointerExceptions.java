package com.example.demo;

import java.awt.*;
import java.util.HashMap;

public class HelpfulNullPointerExceptions {
    /**
     * Exception in thread "main" java.lang.NullPointerException: Cannot invoke "com.example.demo.GrapeClass.getColor()" because the return value of "java.util.HashMap.get(Object)" is null
     */
    public static void main(String[] args) {
        HashMap<String, GrapeClass> grapes = new HashMap<>();
        grapes.put("grape1", new GrapeClass(Color.BLUE, 2));
        grapes.put("grape2", new GrapeClass(Color.white, 4));
        grapes.put("grape3", null);
        var color = ((GrapeClass) grapes.get("grape3")).getColor();
    }
}
