package com.example.demo;

import java.awt.*;

public class PatternMatching {

    private static void oldStyle() {
        Object o = new GrapeClass(Color.BLUE, 2);
        if (o instanceof GrapeClass) {
            GrapeClass grape = (GrapeClass) o;
            System.out.println("This grape has " + grape.getNbrOfPits() + " pits.");
        }
    }

    private static void patternMatching() {
        Object o = new GrapeClass(Color.BLUE, 2);
        if (o instanceof GrapeClass grape && grape.getNbrOfPits() == 2) {
            System.out.println("This grape has " + grape.getNbrOfPits() + " pits.");
        }
    }

    /** Do not compile
        private static void patternMatching() {
            Object o = new GrapeClass(Color.BLUE, 2);
            if (o instanceof GrapeClass grape || grape.getNbrOfPits() == 2) {
                System.out.println("This grape has " + grape.getNbrOfPits() + " pits.");
            }
        }
     */
}
