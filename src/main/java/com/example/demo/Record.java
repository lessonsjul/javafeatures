package com.example.demo;

import java.awt.*;
import java.util.Objects;

public class Record {
    public static void main(String[] args) {
        System.out.println("GrapeClass using the autogenerate functions of your IDE to generate constructor, getters, hashCode, equals and toString:");
        oldStyle();
        System.out.println("----------");
        System.out.println("As a record:");
        basicRecord();
        System.out.println("A record with validation in constructor");
        basicRecordWithValidation();
    }

    private static void oldStyle() {
        GrapeClass grape1 = new GrapeClass(Color.BLUE, 1);
        GrapeClass grape2 = new GrapeClass(Color.WHITE, 2);
        System.out.println("Grape 1 is " + grape1);
        System.out.println("Grape 2 is " + grape2);
        System.out.println("Grape 1 equals grape 2? " + grape1.equals(grape2));
        GrapeClass grape1Copy = new GrapeClass(grape1.getColor(), grape1.getNbrOfPits());
        System.out.println("Grape 1 equals its copy? " + grape1.equals(grape1Copy));
    }

    private static void basicRecord() {
        GrapeRecord grape1 = new GrapeRecord(Color.BLUE, 1);
        GrapeRecord grape2 = new GrapeRecord(Color.WHITE, 2);
        System.out.println("Grape 1 is " + grape1);
        System.out.println("Grape 2 is " + grape2);
        System.out.println("Grape 1 equals grape 2? " + grape1.equals(grape2));
        GrapeRecord grape1Copy = new GrapeRecord(grape1.color(), grape1.nbrOfPits());
        System.out.println("Grape 1 equals its copy? " + grape1.equals(grape1Copy));
    }

    private static void basicRecordWithValidation() {
        GrapeRecordWithValidation grape1 = new GrapeRecordWithValidation(Color.BLUE, 1);
        System.out.println("Grape 1 is " + grape1);
        GrapeRecordWithValidation grapeNull = new GrapeRecordWithValidation(null, 2);
    }
}

class GrapeClass {

    private final Color color;
    private final int nbrOfPits;

    public GrapeClass(Color color, int nbrOfPits) {
        this.color = color;
        this.nbrOfPits = nbrOfPits;
    }

    public Color getColor() {
        return color;
    }

    public int getNbrOfPits() {
        return nbrOfPits;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GrapeClass that = (GrapeClass) o;
        return nbrOfPits == that.nbrOfPits && color.equals(that.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(color, nbrOfPits);
    }

    @Override
    public String toString() {
        return "GrapeClass{" +
                "color=" + color +
                ", nbrOfPits=" + nbrOfPits +
                '}';
    }

}
record GrapeRecord(Color color, int nbrOfPits) {}

record GrapeRecordWithValidation(Color color, int nbrOfPits) {
    GrapeRecordWithValidation {
        System.out.println("Parameter color=" + color + ", Field color=" + this.color());
        System.out.println("Parameter nbrOfPits=" + nbrOfPits + ", Field nbrOfPits=" + this.nbrOfPits());
        if (color == null) {
            throw new IllegalArgumentException("Color may not be null");
        }
    }
}