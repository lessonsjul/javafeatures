package com.example.demo.sealeddemo;

import com.example.demo.sealeddemo.nonsealed.Apple;
import com.example.demo.sealeddemo.nonsealed.Fruit;
import com.example.demo.sealeddemo.nonsealed.Pear;
import com.example.demo.sealeddemo.sealed.AppleSealed;
import com.example.demo.sealeddemo.sealed.FruitSealed;
import com.example.demo.sealeddemo.sealed.PearSealed;

public class SealedClasses {

    public static void main(String[] args) {
        System.out.println("Non sealed classes in other package");
        problemSpace();
        System.out.println("With sealed classes");
        sealedClasses();
    }

    private static void problemSpace() {
        Apple apple = new Apple();
        Pear pear = new Pear();
        Fruit fruit = apple;
        class Avocado extends Fruit {};
    }

    private static void sealedClasses() {
        AppleSealed apple = new AppleSealed();
        PearSealed pear = new PearSealed();
        FruitSealed fruit = apple;
        class Avocado extends AppleSealed {}
//        Not possible extends FruitSealed
    }
}
