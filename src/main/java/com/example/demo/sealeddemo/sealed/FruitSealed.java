package com.example.demo.sealeddemo.sealed;

public abstract sealed class FruitSealed permits AppleSealed, PearSealed {
}
