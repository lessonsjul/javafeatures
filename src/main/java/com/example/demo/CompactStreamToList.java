package com.example.demo;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CompactStreamToList {

    private static void oldStyle() {
        List<String> stringList =  Stream.of("a", "b", "c").collect(Collectors.toList());
    }

    private static void streamToList() {
        List<String> stringList =  Stream.of("a", "b", "c").toList();
    }
}
