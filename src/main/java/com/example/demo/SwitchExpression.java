package com.example.demo;

public class SwitchExpression {

    public static void main(String[] args) {
        System.out.println("Switch with OLD style:");
        withOldSwitchExpression(Fruit.APPLE);
        withOldSwitchExpression(Fruit.ORANGE);
        System.out.println("-----------");
        System.out.println("Switch with NEW style:");
        withSwitchExpression(Fruit.APPLE);
        withSwitchExpression(Fruit.ORANGE);
    }

    private static void withSwitchExpression(Fruit fruit) {
        switch (fruit) {
            case APPLE, PEAR -> System.out.println("Common fruit");
            case ORANGE, AVOCADO -> System.out.println("Exotic fruit");
            default -> System.out.println("Undefined fruit");
        }
    }

    private static void withOldSwitchExpression(Fruit fruit) {
        switch (fruit) {
            case APPLE, PEAR:
                System.out.println("Common fruit");
                break;
            case ORANGE, AVOCADO:
                System.out.println("Exotic fruit");
            default:
                System.out.println("Undefined fruit");
        }
    }

    private enum Fruit {
        APPLE, PEAR, ORANGE, AVOCADO
    }
}
