package com.example.demo;

public class TextBlocks {

    public static void main(String[] args) {
        String withPrecedingSpaces = """
                   {
                       "name": "John Doe",
                       "age": 45,
                       "address": "Doe Street, 23, Java Town"
                   }
                """;

        System.out.println("With preceding spaces:");
        System.out.println(withPrecedingSpaces);

        System.out.println("-----------");

        String noPrecedingSpaces = """
                   {
                       "name": "John Doe",
                       "age": 45,
                       "address": "Doe Street, 23, Java Town"
                   }
                    """;

        System.out.println("Without preceding spaces:");
        System.out.println(noPrecedingSpaces);
    }
}
