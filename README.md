# New Features in Java 17

### Removed:
* The feature of the security manager
* JIT compiler and experimental AOT

### Are available:
* Generate Sealed classes
* The feature of switch pattern matching
* Stream.toList()
* Helpful NulPointerException
* Record type - immutable data class
* Switch expression with pattern matching
* Untyped variable `var`
* Text Blocks that allows multiline string

## Foreign Function and Memory API (Incubator)

The Foreign Function and Memory API allow Java developers to access code from outside the JVM and manage memory out of the heap. The goal is to replace the JNI API and improve the security and performance compared to the old one.

## Vector API (Second Incubator)

The Vector API deals with the SIMD (Single Instruction, Multiple Data) type of operation, meaning various sets of instructions executed in parallel. It leverages specialized CPU hardware that supports vector instructions and allows the execution of such instructions as pipelines.

As a result, the new API will enable developers to implement more efficient code, leveraging the potential of the underlying hardware.

Everyday use cases for this operation are scientific algebra linear applications, image processing, character processing, and any heavy arithmetic application or any application that needs to apply an operation for multiple independent operands.

# The rendering pipeline of MacOS
This JEP implements a Java 2D internal rendering pipeline for macOS since Apple deprecated the OpenGL API (in macOS 10.14), used internally in Swing GUI. The new implementation uses the Apple Metal API, and apart from the internal engine, there were no changes to the existing API


### Links:
* https://www.educba.com/java-11-vs-java-17/
* https://www.baeldung.com/java-17-new-features
* https://dzone.com/articles/whats-new-between-java-11-and-java-17